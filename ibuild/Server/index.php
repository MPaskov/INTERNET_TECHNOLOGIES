<?php
 header("Access-Control-Allow-Origin: *");
 header("Access-Control-Allow-Methods: PUT, GET, POST");
 header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Origin, Cache-Control, Pragma, Authorization, Accept, Accept-Encoding");


include_once "Adapter/PDODatabase.php";
include_once "Adapter/PDODatabaseStatement.php";
include_once "config.php";

$db = new \Adapter\PDODatabase(
    DB_HOST,
    DB_NAME,
    DB_USER,
    DB_PASSWORD
);

$_POST = json_decode(file_get_contents("php://input"), true);
$_REQUEST = json_decode(file_get_contents("php://input"), true);

if($_REQUEST['apiCall'] === 'deleteItem')
{
	$id = $_REQUEST['id'];

	$stmt = $db->prepare("
                DELETE FROM items WHERE id = ?
	");
	$stmt->execute(
		[
			$id
		]
	);

	echo TRUE;
}

if ($_POST['apiCall'] == 'login')
{
	$email = $_POST['email'];
	$password = $_POST['password'];

	$stmt = $db->prepare("
                SELECT * FROM users WHERE email = ?
	");
	$stmt->execute(
		[
			$email
		]
	);
	$user = $stmt->fetchRow();
	if ($user) {
		// verifying user password
		$encrypt_password = $user['password'];

		// check for password equality
		if ($encrypt_password == md5('root' . $password)) {
			// user authentication details are correct

		    echo json_encode($user);
		}
	} else {
		echo "No user found!";
	}
}
else if ($_POST['apiCall'] === 'register')
{
	$username = $_POST['username'];
	$email = $_POST['email'];
	$password = md5('root' . $_POST['password']);
	$role = "Admin";

	$stmt = $db->prepare("
		INSERT INTO users
		SET
		username = :username,
		password = :password,
		email = :email,
		role = :role
	");
	$stmt->execute(
		[
			"username" => $username,
			"password" => $password,
			"role" => $role,
			"email" => $email
		]
	);
	$stmt = $db->prepare("
                SELECT * FROM users WHERE email = ?
	");
	$stmt->execute(
		[
			$email
		]
	);
	$user = $stmt->fetchRow();
	echo json_encode($user);
}
else if ($_POST['apiCall'] === 'addItem')
{
	$category = $_POST['category'];
	$price = $_POST['price'];
	$name = $_POST['name'];
	$description = $_POST['description'];
	$quantity = $_POST['quantity'];


	$stmt = $db->prepare("
		INSERT INTO items
		SET
		category = :category,
		price = :price,
		name = :name,
		description = :description,
		quantity = :quantity
	");
	$stmt->execute(
		[
			"category" => $category,
			"price" => $price,
			"name" => $name,
			"description" => $description,
			"quantity" => $quantity
		]
	);

}
else if($_GET['apiCall'] === 'itemsCH')
{
	$stmt = $db->prepare("
                SELECT * FROM items WHERE category = 'ConstructionAndHardware'
	");
	$stmt->execute();
	$items = $stmt->fetchAll(\PDO::FETCH_ASSOC);

	echo json_encode($items);

}else if($_GET['apiCall'] === 'itemsEAH')
{
	$stmt = $db->prepare("
                SELECT * FROM items WHERE category = 'ElectricalAppliancesAndHeating'
	");
	$stmt->execute();
	$items = $stmt->fetchAll(\PDO::FETCH_ASSOC);

	var_dump($items);
	echo json_encode($items);
}else if($_GET['apiCall'] === 'itemsID')
{
	$stmt = $db->prepare("
                SELECT * FROM items WHERE category = 'InteriorAndDecoration'
	");
	$stmt->execute();
	$items = $stmt->fetchAll(\PDO::FETCH_ASSOC);
	echo json_encode($items);
}else if($_GET['apiCall'] === 'itemsTE')
{
	$stmt = $db->prepare("
                SELECT * FROM items WHERE category = 'ToolsAndEquipment'
	");
	$stmt->execute();
	$items = $stmt->fetchAll(\PDO::FETCH_ASSOC);
	echo json_encode($items);
}else if($_GET['apiCall'] === 'itemsYG')
{
	$stmt = $db->prepare("
                SELECT * FROM items WHERE category = 'YardAndGarden'
	");
	$stmt->execute();
	$items = $stmt->fetchAll(\PDO::FETCH_ASSOC);
	echo json_encode($items);
}else if($_GET['apiCall'] === 'allItems')
{
	$stmt = $db->prepare("
                SELECT * FROM items
	");
	$stmt->execute();
	$items = $stmt->fetchAll(\PDO::FETCH_ASSOC);
	echo json_encode($items);
}else if($_GET['apiCall'] === 'selectedItem')
{
	$id = $_GET['id'];
	$stmt = $db->prepare("
                SELECT * FROM items WHERE id = ?
	");
	$stmt->execute(
		[
			$id
		]
	);
	$item = $stmt->fetchRow();
	echo json_encode($item);
}else if($_GET['apiCall'] === 'itemsRand')
{
	$stmt = $db->prepare("
	SELECT *
	FROM items
	ORDER BY RAND()
	");
	$stmt->execute();
	$items = $stmt->fetchAll(\PDO::FETCH_ASSOC);
	echo json_encode($items);
}