<?php

namespace Adapter;

include "DatabaseStatementInterface.php";

class PDODatabaseStatement implements DatabaseStatementInterface
{
    /**
     * @var \PDOStatement
     */
    private $statement;

    function __construct(\PDOStatement $statement)
    {
        $this->statement = $statement;
    }

    public function execute(array $params = [])
    {
        return $this->statement->execute($params);
    }

    public function fetchRow()
    {
        return $this->statement->fetch(\PDO::FETCH_ASSOC);
    }

    public function fetchAll($fetch_style)
    {
        return $this->statement->fetchAll($fetch_style);
    }

    public function fetchObject($className)
    {
        return $this->statement->fetchObject($className);
    }

    public function rowCount()
    {
        return $this->statement->rowCount();
    }
}