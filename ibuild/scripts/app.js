function startApp() {

    showMenuHideLinks()
    showHomeView()
    // Navigation menu links 
    $('#linkHome').click(showHomeView)
    $('#linkLogin').click(showLoginView)
    $('#linkRegister').click(showRegisterView)
    $('#linkShop').click(showShopView)
    $('#linkCart').click(showCartView)
    $('#linkLogout').click(logoutUser)
    $('#linkAddNewItem').click(showViewAdminAddItem)
    $('#linkRemoveItem').click(showViewAdminRemoveItem)
    
    

    // buttons
    $('#linkForgotPass').click(showForgotPass)
    $('#linkForgotPassNewAccount').click(showRegisterView)
    $('#getNewPass').click(mailTo) 
    $('#backToLogin').click(showLoginView)   
    $('#btnContinueShopping').click(showShopView)
    $("#submitLogin").click(login)
    $("#submitRegister").click(register)


    // sub-nav-shop links
    $('#linkConstructionHardware').click(showConstructionHardware)
    $('#linkElectrivalAppliancesHeating').click(showElectrivalAppliancesHeating)
    $('#linkInteriorDecoration').click(showInteriorDecoration) 
    $('#linkToolsEquipment').click(showToolsEquipment)   
    $('#linkYardGarden').click(showYardGarden)
    
    // footer links
    $('#linkLoginF').click(showLoginView)
    $('#linkHomeF').click(showHomeView)
    $('#linkRegisterF').click(showRegisterView)
    $('#linkShopF').click(showShopView)
    $('#linkUserAccount').click(showUserAccount)

    //other buttons
    $('#btnAddNewItem').click(addNewItem)


    

}