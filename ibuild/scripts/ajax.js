function listItems(category) {
    $('#items').empty();
    showView('viewShop');


    switch (category) {
        case "ConstructionAndHardware": {
            $.ajax({
                    method: 'GET',
                    contentType: 'application/json',
                    url: "http://localhost/ibuild/Server/index.php?",
                    data: {
                        apiCall: 'itemsCH'
                    }
                }).then(loadAdsSuccess)
                .catch(handleAjaxError);
            break;
        }
        case "ElectricalAppliancesAndHeating": {
            $.ajax({
                    method: 'GET',
                    contentType: 'application/json',
                    url: "http://localhost/ibuild/Server/index.php?",
                    data: {
                        apiCall: 'itemsEAH'
                    }
                }).then(loadAdsSuccess)
                .catch(handleAjaxError);
            break;
        }
        case "InteriorAndDecoration": {
            $.ajax({
                    method: 'GET',
                    contentType: 'application/json',
                    url: "http://localhost/ibuild/Server/index.php?",
                    data: {
                        apiCall: 'itemsID'
                    }
                }).then(loadAdsSuccess)
                .catch(handleAjaxError);
            break;
        }
        case "ToolsAndEquipment": {
            $.ajax({
                    method: 'GET',
                    contentType: 'application/json',
                    url: "http://localhost/ibuild/Server/index.php?",
                    data: {
                        apiCall: 'itemsTE'
                    }
                }).then(loadAdsSuccess)
                .catch(handleAjaxError);
            break;
        }
        case "YardAndGarden": {
            $.ajax({
                    method: 'GET',
                    contentType: 'application/json',
                    url: "http://localhost/ibuild/Server/index.php?",
                    data: {
                        apiCall: 'itemsYG'
                    }
                }).then(loadAdsSuccess)
                .catch(handleAjaxError);
            break;
        }
        default:
            console.error("Cant find category!");

    }

}

function listHomeItems() {

    $.ajax({
            method: 'GET',
            contentType: 'application/json',
            url: "http://localhost/ibuild/Server/index.php?",
            data: {
                apiCall: 'itemsRand'
            }
        }).then(getCountSuccess)
        .catch(handleAjaxError);

    function getCountSuccess(items) {
        console.log(items);
        items = JSON.parse(items);
        console.log(typeof items)
        console.log(items);

        if (items.length === 0) {
            $('#items').css({
                "font-size": "xx-large",
                "padding": "10px"
            }).text('No items in the list.')
        } else {
            showInfo('Items loaded success!.');
            let container = $('#homeBox');
            $('#homeBox').empty();
            for (let item of items) {
                appendItemHome(item, container);
            }
        }

        function appendItemHome(item, container) {
            let moreInfo = $('<button class="btn btn-outline-primary m-2">More Info</button>')
                .click(displayitem.bind(this, item.id));
            let buy = $('<button class="btn btn-outline-success m-2">Buy</button>')
                .click(addToCart.bind(this, item.id));

            let div = $('<div class="col-md-4" >').append(
                $('<div class="card m-3 p-2 pr-3  bg-light" >').append(

                    $('<div class="card-img-top">').append($('<img class="card-img-top">').attr({
                        src: "pictures/mouse.jpg",
                        width: "220px",
                        height: "200px"
                    })),
                    $('<div class="card-block p-2 ">').append(
                        $('<h4>').text(item.name),
                        $('<div class="card-text d-inline-block float-left text-danger">').text("Price: " + item.price + " lv"),
                        $('<div class="row justify-content-end d-inline-block float-right">').append(
                            moreInfo,
                            buy
                        ))));




            container.append(div);



        }

        function displayitem(id) {


            $('#homeBox').empty();
            console.log("DisplayItem id: " + id)
            $.ajax({
                    method: 'GET',
                    contentType: 'application/json',
                    url: "http://localhost/ibuild/Server/index.php?",
                    data: {
                        apiCall: 'selectedItem',
                        id: id
                    }
                }).then(loadItemSuccessed)
                .catch(handleAjaxError);

            window.scroll(0, 0);

            function loadItemSuccessed(item) {
                container = $('#homeBox');
                item = JSON.parse(item);
                appendSelectedItem(item, container);

                function appendSelectedItem(item, container) {
                    let btn = $('<button type="button" class="btn btn-outline-success d-inline-block">Add to cart</button>');

                    let div2 = $('<div class="row">').append(
                        $('<div class="col-md-12 col-sm-12 ">').append(
                            $('<h3 class="text-uppercase text-muted text-center">').text(item.name),
                            $('<hr>')
                        ),
                        $('<div class="row rol-col-10 w-50">').append(

                            $('<div class="col-md-10 col-sm-10 p-2 ">').append(
                                $('<span class="product-name ">').text("Category:  "),
                                $('<span class="product-name">').text(item.category),
                                $('<hr>')

                            ),
                            $('<div class="col-md-10 col-sm-10 p-2 ">').append(
                                $('<span class="product-name ">').text("Price for 1 item:   "),
                                $('<span class="product-name ">').text(item.price),
                                $('<hr>')

                            ),
                            $('<div class="col-md-10 col-sm-10 p-2 ">').append(
                                $('<span class="product-name ">').text("Quantity:   "),
                                $('<span class="product-name ">').text(item.quantity),
                                $('<hr>')

                            ),
                            $('<div class="col-md-10 col-sm-10 p-2 ">').append(
                                $('<p class="product-name">').text("Description:   "),
                                $('<p class="text-justify ">').text("asdasdasdasd asdasljbfjwdhfba lerhbfaeulrhbfuae khrbfakhu rbfaeouhrb faouehbrfuaehbrfoa euhbrfkahbr fuaekhbfr akuehrbfaurfr")

                            ),

                        ),
                        $('<div class="card m-3  p-3  bg-light ">').append(
                            $('<img class="img-responsive  " >').attr({
                                src: "pictures/mouse.jpg",
                                width: "400px",
                                height: "400px"
                            })
                        ),
                        $('<div class= row-2 >').append(
                            $('<div class= col-sm-12>').append(btn)
                        )

                    );
                    container.append(div2);

                }
            }

        }
    }

}
let container = $('#items');

function loadAdsSuccess(items) {
    console.log(items);
    $('#items').empty();
    items = JSON.parse(items);
    console.log(typeof items)

    if (items.length === 0) {
        $('#items').css({
            "font-size": "xx-large",
            "padding": "10px"
        }).text('No items in the list.')
    } else {
        showInfo('Items loaded success!.');
        let container = $('#items');

        for (let item of items) {
            appendItem(item, container);
        }
    }

    function appendItem(item, container) {
        let moreInfo = $('<button class="btn btn-outline-primary m-2">More Info</button>')
            .click(displayitem.bind(this, item.id));
        let buy = $('<button class="btn btn-outline-success m-2">Buy</button>')
            .click(addToCart.bind(this, item.id));



        let div = $('<div class="col-md-4" >').append(
            $('<div class="card m-3 p-2 pr-3  bg-light" >').append(

                $('<div class="card-img-top">').append($('<img class="card-img-top">').attr({
                    src: "pictures/mouse.jpg",
                    width: "220px",
                    height: "200px"
                })),
                $('<div class="card-block p-2 ">').append(
                    $('<h4>').text(item.name),
                    $('<div class="card-text d-inline-block float-left text-danger">').text("Price: " + item.price + " lv"),
                    $('<div class="row justify-content-end d-inline-block float-right">').append(
                        moreInfo,
                        buy
                    ))

            ));
        container.append(div);

        function displayitem(id) {


            $('#items').empty();
            console.log("DisplayItem id: " + id)
            $.ajax({
                    method: 'GET',
                    contentType: 'application/json',
                    url: "http://localhost/ibuild/Server/index.php?",
                    data: {
                        apiCall: 'selectedItem',
                        id: id
                    }
                }).then(loadItemSuccessed)
                .catch(handleAjaxError);

            window.scroll(0, 0);

            function loadItemSuccessed(item) {
                container = $('#item');
                item = JSON.parse(item);
                appendSelectedItem(item, container);

                function appendSelectedItem(item, container) {
                    let btn = $('<button type="button" class="btn btn-outline-success d-inline-block">Add to cart</button>')
                        .click(addToCart.bind(this, item.id));

                    let div2 = $('<div class="row">').append(
                        $('<div class="col-md-12 col-sm-12 ">').append(
                            $('<h3 class="text-uppercase text-muted text-center">').text(item.name),
                            $('<hr>')
                        ),
                        $('<div class="row rol-col-10 w-50">').append(

                            $('<div class="col-md-10 col-sm-10 p-2 ">').append(
                                $('<span class="product-name ">').text("Category:  "),
                                $('<span class="product-name">').text(item.category),
                                $('<hr>')

                            ),
                            $('<div class="col-md-10 col-sm-10 p-2 ">').append(
                                $('<span class="product-name ">').text("Price for 1 item:   "),
                                $('<span class="product-name ">').text(item.price),
                                $('<hr>')

                            ),
                            $('<div class="col-md-10 col-sm-10 p-2 ">').append(
                                $('<span class="product-name ">').text("Quantity:   "),
                                $('<span class="product-name ">').text(item.quantity),
                                $('<hr>')

                            ),
                            $('<div class="col-md-10 col-sm-10 p-2 ">').append(
                                $('<p class="product-name">').text("Description:   "),
                                $('<p class="text-justify ">').text("asdasdasdasd asdasljbfjwdhfba lerhbfaeulrhbfuae khrbfakhu rbfaeouhrb faouehbrfuaehbrfoa euhbrfkahbr fuaekhbfr akuehrbfaurfr")

                            ),

                        ),
                        $('<div class="card m-3  p-3  bg-light">').append(
                            $('<img class="img-responsive  " >').attr({
                                src: "pictures/mouse.jpg",
                                width: "400px",
                                height: "400px"
                            })
                        ),
                        btn
                    );
                    container.append(div2);

                }
            }

        }



    }

}

function loadAllItems() {
    $('#removeItem').empty();
    showView('adminRemoveItem');

    $.ajax({
            method: 'GET',
            contentType: 'application/json',
            url: "http://localhost/ibuild/Server/index.php?",
            data: {
                apiCall: 'allItems'
            }
        }).then(loadItemsSuccess)
        .catch(handleAjaxError);

    function loadItemsSuccess(items) {
        items = JSON.parse(items);
        console.log(typeof items)

        if (items.length === 0) {
            $('#items').css({
                "font-size": "xx-large",
                "padding": "10px"
            }).text('No items in the list.')
        } else {
            showInfo('Items loaded success!.');
            let container = $('#removeItem');

            for (let item of items) {
                appendAllItems(item, container);
            }
        }

        function appendAllItems(item, container) {
            let deleteItem = $('<button class="btn btn-outline-danger m-2">Remove</button>')
                .click(removeThisItem.bind(this, item.id));




            let div = $('<div class="col-md-4" >').append(
                $('<div class="card m-3 p-2 pr-3  bg-light" >').append(

                    $('<div class="card-img-top">').append($('<img class="card-img-top">').attr({
                        src: "pictures/mouse.jpg",
                        width: "220px",
                        height: "200px"
                    })),
                    $('<div class="card-block p-2 ">').append(
                        $('<h4>').text(item.name),
                        $('<div class="card-text d-inline-block float-left text-danger">').text("Price: " + item.price + " lv"),
                        $('<div class="row justify-content-end d-inline-block float-right">').append(
                            deleteItem
                        ))

                ));
            container.append(div);
        }

        function removeThisItem(id) {
            console.log("DisplayItem remove id: " + id)
            $.post("http://localhost/ibuild/Server/index.php?", {
                    apiCall: 'deleteItem',
                    'id': id
                })
                .done(removeSuccess)
                .fail(handleAjaxError)


        }
        removeSuccess = () => {
            showInfo("Remove success!")
            showViewAdminRemoveItem();
        }
    }


}

function addToCart(id) {
    showCartView();
    $('#cartContainer').empty();
    console.log("DisplayItem id: " + id)
    $.ajax({
            method: 'GET',
            contentType: 'application/json',
            url: "http://localhost/ibuild/Server/index.php?",
            data: {
                apiCall: 'selectedItem',
                id: id
            }
        }).then(addSuccess)
        .catch(handleAjaxError);

    function addSuccess(item) {
        container = $('#cartContainer');
        item = JSON.parse(item);
        appendThisItem(item, container);

        function appendThisItem() {
            let btnContinue = $('<button id="btnContinueShopping" type="button" class="btn btn-outline-primary">Continue Shopping</button>')
                .click();



            let div3 = $('<div class="row">').append(
                $('<div class="col-sm-12 col-md-10 col-md-offset-1">').append(
                    $('<table class="table table-hover">').append(
                        $('<thead>').append(
                            $('<tr>').append(
                                $('<th>').text("Product"),
                                $('<th>').text("Quantity"),
                                $('<th class="text-center>').text("Price"),
                                $('<th class="text-center>').text("Total"),
                            )
                        ),
                        $('<tbody>').append(
                            $('<tr>').append(
                                $('<td class="col-sm-8 col-md-6">').append(
                                    $('<div class="media">').append(
                                        $('<a class="thumbnail pull-left href="#"">').append(
                                            $('<img class="media-object" src="scripts/mouse.jpg" style="width: 72px; height: 72px;"></img>')
                                        ),
                                        $('<div class="media-body">').append(
                                            $('<h4 class="media-heading">').text(item.name),
                                            $('<span>').text("In Stock:"),
                                            $('<span class="text-success">').text(item.quantity)
                                        )
                                    )
                                ),
                                $('<td class="col-sm-1 col-md-1" style="text-align: center">').append(
                                    $('<input type="numberItems" class="form-control" id="countOfItem" value="input">')
                                ),
                                $('<td class="col-sm-1 col-md-1 text-center">').append(
                                    $('<strong>').text(item.price)
                                ),
                                $('<td class="col-sm-1 col-md-1 text-center">').append(
                                    $('<strong id="totalPrice">').text(item.price * $('#numberItems').val())
                                )
                            ),
                            $('<tr>').append(
                                $('<td>'),
                                $('<td>'),
                                $('<td>'),
                                $('<td>').append($('<h5>').text("Total price:")),
                                $('<td>').append(($('<h5>').text($('#totalPrice')))),
                                $('<tr>').append(
                                    $('<td>'),
                                    $('<td>'),
                                    $('<td>'),
                                    $('<td>').append(
                                        btnContinue

                                    ),
                                    $('<td>').append(
                                        $('<button type="button" class="btn btn-outline-success ">').append(
                                            $('<span class="glyphicon glyphicon-shopping-cart">').text("Check out")
                                        )
                                    )
                                )


                            )
                        )
                    )
                )

            )



            container.append(div3);

        }
    }
}